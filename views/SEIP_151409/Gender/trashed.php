<?php
require_once("../../../vendor/autoload.php");
use App\BITM\SEIP_151409\Message\Message;
if(!isset( $_SESSION)) session_start();
$message1=Message::message();



use App\BITM\SEIP_151409\Gender\Gender;

$obj= new Gender();


$all_person= $obj->trashed();
######################## pagination code block#1 of 2 start ######################################
$recordCount= count($all_person);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $obj->trashPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;


####################### pagination code block#1 of 2 end #########################################

?>
<!--table-->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title> </title>

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="../../../resource/Bootstrap/css/bootstrap-theme.min.css">
    <!-- <link rel="stylesheet" href="../../../resource/Bootstrap/css/booktitle.css"> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>

<style>
    .main{
        margin-top: 10%;
        margin-left: 15%;
        margin-right:15%;
        background-color: #679a9f;



    }
    body{
        background-image:url("../../../resource/assets/images/general3.jpg");

        background-repeat:no-repeat;
        background-size: 100% 925px;

    }



</style>

<body  >
<div class="container ">
    <div style="margin-top: 40px ;float: right;"> <a href="../index.php" class="btn btn-info btn-danger btn-lg" role="button">Atomic Project List</a></br></br></div>

    <div class="main">



        <div class="panel panel-default" >
            <div class="panel-heading">
                <div class="panel-heading">
                    <h1 style="text-align: center"> Trashed List</h1>


                </div>
            </div>





            <div class="panel-body">
                <form action="recovermultiple.php" method="post" id="multiple">
                <div class="table-responsive" >
                    </br></br></br></br>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Select</th>
                            <th>#</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Gender</th>

                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial=0;

                            foreach($someData as $result){
                            $serial++; ?>
                            <td><input type="checkbox" name="mark[]" value="<?php echo $result['id']?>"></td>
                            <td><?php echo $serial?></td>
                            <td><?php echo $result['id']?></td>
                            <td><?php echo $result['name']?></td>
                            <td><?php echo $result['gender']?></td>
                            <td><a href="recover.php?id=<?php echo $result['id']  ?>" class="btn btn-primary" role="button">Recover</a>

                                <a href="delete.php?id=<?php echo $result['id'] ?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>

                            </td>

                        </tr>
                        <?php }?>




                        </tbody>
                        <a href="index.php"  class="btn btn-info" role="button">Home</a> &nbsp;&nbsp;&nbsp;&nbsp;

                        <button type="submit" class="btn btn-info">Recover Selected</button>&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-primary" id="delete">Delete all Selected</button>
                    </table>
                    <!--  ######################## pagination code block#2 of 2 start ###################################### -->
                    <div align="left" class="container">
                        <ul class="pagination">

                            <?php
                            $previous=$page-1;
                            if($previous>0)
                            {echo "<li><a href='index.php?Page=$previous'>" . "Previous" . '</a></li>';}
                            for($i=1;$i<=$pages;$i++)
                            {
                                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                            }
                            $next=$page+1;
                            if($next<=$pages)
                                echo "<li><a href='index.php?Page=$next'>" . "Next" . '</a></li>';
                            ?>

                            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                                <?php
                                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
                                else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

                                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
                                else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

                                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
                                else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

                                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                                ?>
                            </select>
                        </ul>
                    </div>
                    <!--  ######################## pagination code block#2 of 2 end ###################################### -->

                    <div id="confirmation_message" style="color:red;">
                        <?php echo $message1 ?>
                    </div>

                </div>
                    </form>

            </div>

        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $(function() {
            $('#confirmation_message').delay(3000).fadeOut();

        });

    });

    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
    $('#delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });
</script>


<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>



