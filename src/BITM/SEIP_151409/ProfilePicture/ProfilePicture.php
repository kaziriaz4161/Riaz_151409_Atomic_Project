<?php
namespace App\BITM\SEIP_151409\ProfilePicture;
useApp\BITM\SEIP_151409\Message\Message;
use App\BITM\SEIP_151409\Utility\Utility;
use App\BITM\SEIP_151409\Model\Database as DB;
use PDO;

class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $picture_address="";



    public function __construct(){
        parent::__construct();
    }


    public function index($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture where is_deleted='0'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);

        $all_picture=$sth->fetchAll();


        return  $all_picture;
    }







    public function view($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture Where id=$id");
        $sth->execute();
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $selected_person=$sth->fetch();

        return  $selected_person;

    }



    public function setData($data=null){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];

        }
        if(array_key_exists('picture_address',$data)){
            $this->picture_address=$data['picture_address'];

        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->picture_address);
        $query="insert into profile_picture(name,picture_address) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>[ Name: $this->name ] ,
               [ profile picture: $this->picture_address ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');



    }


    public function update(){
        $dbh=$this->connection;
        $values=array($this->name,$this->picture_address);

        //var_dump($values);


        $query='UPDATE profile_picture SET  name = ?   , picture_address = ? where id ='.$this->id;



        //    $query='UPDATE book_title  SET book_title  = ?   , author_name = ? where id ='.$this->id;

        $sth=$dbh->prepare($query);
        $sth->execute($values);
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
                <br> Data Has Been Updated Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Updated !</h3></div>");
        Utility::redirect('index.php');



    }
   public function delete($id){
        $DBH=$this->connection;
        $sth=$DBH->prepare("delete  from profile_picture Where id=$id");
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has deleted Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been Inserted !</h3></div>");
        Utility::redirect('index.php');


    }
    public function trash($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE profile_picture  SET is_deleted  = "1" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been trashed Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
        Utility::redirect('index.php');



    }
    public function trashed($fetchMode='ASSOC'){
        $fetchMode = strtoupper($fetchMode);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture WHERE is_deleted='1'");
        $sth->execute();
        if(substr_count($fetchMode,'OBJ') > 0)
            $sth->setFetchMode(PDO::FETCH_OBJ);
        else
            $sth->setFetchMode(PDO::FETCH_ASSOC);


        $all_person=$sth->fetchAll();

        return  $all_person;
    }

    public function recover($id){
        $dbh=$this->connection;


        //var_dump($values);


        $query='UPDATE profile_picture  SET is_deleted  = "0" where id ='.$id;

        $sth=$dbh->prepare($query);
        $sth->execute();
        if($sth)
        { Message::message("<div id='msg'><h3 align='center'>
               <br> Data Has Been recovered Successfully!</h3></div>");
        }

        else
            Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
        Utility::redirect('trashed.php');



    }
    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $dbh=$this->connection;
            $n=count($IDs);
            // Utility::dd($IDs);
            for($i=0;$i<$n;$i++)
            {
                $query="delete  from profile_picture Where id=$IDs[$i]";
                $sth=$dbh->prepare($query);
                $sth->execute();
            }






            if($sth)
            { Message::message("<div id='msg'><h3 align='center'>
               <br> Selected Data has been deleted Successfully!</h3></div>");
            }

            else
                Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
            Utility::redirect('index.php');



        }
    }
    public function recoverMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $dbh=$this->connection;
            $n=count($IDs);
            // Utility::dd($IDs);
            for($i=0;$i<$n;$i++)
            {
                $query='UPDATE profile_picture  SET is_deleted  = "0" where id ='.$IDs["$i"];
                $sth=$dbh->prepare($query);
                $sth->execute();
            }






            if($sth)
            { Message::message("<div id='msg'><h3 align='center'>
               <br> Selected Data has been deleted Successfully!</h3></div>");
            }

            else
                Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been recovered !</h3></div>");
            Utility::redirect('index.php');



        }

    }
    public function trashMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $dbh=$this->connection;
            $n=count($IDs);
            // Utility::dd($IDs);
            for($i=0;$i<$n;$i++)
            {
                $query='UPDATE profile_picture  SET is_deleted  = "1" where id ='.$IDs["$i"];
                $sth=$dbh->prepare($query);
                $sth->execute();
            }






            if($sth)
            { Message::message("<div id='msg'><h3 align='center'>
               <br> Selected Data has been Trashed Successfully!</h3></div>");
            }

            else
                Message::message("<div id='msg'></div><h3 align='center'> <br> Data Hasn't Been trashed !</h3></div>");
            Utility::redirect('index.php');



        }

    }
    public function indexPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture WHERE is_deleted='0' LIMIT $start,$itemsPerPage");
        $sth->execute();

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $arrSomeData  = $sth->fetchAll();

        return $arrSomeData;

    }// end of indexPaginator();
    public function trashPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);
        $DBH=$this->connection;
        $sth=$DBH->prepare("select * from profile_picture WHERE is_deleted='1' LIMIT $start,$itemsPerPage");
        $sth->execute();

        $sth->setFetchMode(PDO::FETCH_ASSOC);

        $arrSomeData  = $sth->fetchAll();

        return $arrSomeData;

    }// end of indexPaginator();


    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM profile_picture WHERE is_deleted='0'";
        $DBH=$this->connection;

        $STH = $DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData['name']);
        }

        $STH = $DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData['name']);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData['picture_address']);
        }
        $STH = $DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData['picture_address']);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        return array_unique($_allKeywords);


    }// get all keywords
    public function search($requestArray){
        $sql = "";
        $DBH=$this->connection;
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='0' AND (`name` LIKE '%".$requestArray['search']."%' OR `picture_address` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='0' AND `name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `profile_picture` WHERE `is_deleted` ='0' AND `picture_address` LIKE '%".$requestArray['search']."%'";

        $STH  = $DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $allData = $STH->fetchAll();

        return $allData;

    }// end of search()

}

